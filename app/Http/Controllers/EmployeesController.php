<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Employees;
use App\Companies;
use Illuminate\Validation\Rule;

class EmployeesController extends Controller
{
    public function index($id='',Request $request)
    {
		$employees = Employees::with('company')->orderBy('first_name', 'asc')->get();
		//return $employees;
		return view('pages.employee.list', compact('employees'));
	}
	
	 public function store(Request $request)
    {
	//return $request;
		$id = 0;
         if($request->id > 0 ){
            $id = $request->id;
         }
		try{
			$validator = Validator::make($request->all(), [
				'first_name' => 'required',
				'last_name' => 'required',
				'company_id' => 'required',
				'email' => 'required',
				'phone' => 'required',
			]);
			if ($validator->fails()) {
				return array(
					'error' => true,
					'message' => $validator->errors()
				);
			}
			if($id > 0){
				$item = Employees::findOrFail($request->id);
				if($item){
					Employees::where('id', $request->id)->update(['first_name'=>$request->first_name,'last_name'=>$request->last_name,'company_id'=>$request->company_id,'email'=>$request->email,'phone'=>$request->phone]);
				}
			}else{
				$res = Employees::firstOrCreate([
					'first_name' => $request->first_name,
					'last_name' => $request->last_name,
					'company_id' => $request->company_id,
					'email' => $request->email,
					'phone' => $request->phone,
				]);		
			}
			return redirect()->to('employee');
		}catch(\Illuminate\Database\QueryException $ex){
						$res['status'] = false;
						$res['message'] = $ex->getMessage();
						return response($res, 500);
		};
        //
    }
	
	public function create(){
		$companies = Companies::get();
		return view('pages.employee.add', compact('companies'));
	}
	
	public function edit($id){
		$employee = Employees::find($id);
		$companies = Companies::get();
			return view('pages.employee.add', compact('companies','employee'));
	}
	
	public function destroy($id, Request $request){
		Employees::where('id',$request->deleteid)->delete();
		return redirect()->to('employee');
	}
}
