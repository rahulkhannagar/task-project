<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function(){
Route::get('/employee', 'EmployeesController@index')->name('employee');
Route::get('/employee/create', 'EmployeesController@create')->name('create');
Route::post('/employee/store', 'EmployeesController@store')->name('store');
Route::get('/employee/{id}', 'EmployeesController@edit')->name('employeeedit');
Route::delete('/employee/{id}', 'EmployeesController@destroy')->name('destroy');

Route::get('/company', 'CompaniesController@index')->name('company');
Route::get('/company/{id}', 'CompaniesController@edit')->name('companyedit');

});
//Route::get('/employee/{id}', 'EmployeesController@index')->name('employee');
