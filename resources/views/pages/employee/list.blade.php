@extends('layouts.layout')
@section('title', 'Employee :: List')


    @section('content')
	<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                   <img src="{{ url('/img/logo.png') }}" height="50px;">
                </a>
                <a class="navbar-brand" href="{{ action('CompaniesController@index') }}">
                   Company
                </a>
                <a class="navbar-brand" href="{{ action('EmployeesController@index') }}">
                   Employee
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
	 <a href="{{action('EmployeesController@create')}}" class="buttonNext btn btn-success navbar-right">Add Employee</a>
		<table id="Service-datatable" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
			<thead>
				<tr class="headings">
					<th class="text_color">First Name</th>                                       
					<th class="text_color">Last Name</th>
					<th class="text_color">Company</th>
					<th class="text_color">Email</th>
					<th class="text_color">Phone</th>
					<th class="text_color no-link last"><span class="nobr">Action</span></th>
				</tr>
			</thead>
			
			<tbody>
				@foreach($employees as $employee)
				<tr class="even">
					<td class="a-center ">{{ $employee->first_name}}</td>
					<td class="a-center ">{{ $employee->last_name}}</td>
					<td class="a-center ">{{ $employee->company->name}}</td>
					<td class="a-center ">{{ $employee->email}}</td>
					<td class="a-center ">{{ $employee->phone}}</td>

					<td class=" last">                                     
					
					<a href="{{ action('EmployeesController@edit', $employee->id )}}" class="btn btn-info btn-xs table_btn"> Edit</a>	
					<a href="#" class="btn btn-danger btn-xs table_btn deleteitem" data-id="{{$employee->id}}" data-toggle="modal" data-target="#deleteItem"><i class="fa fa-folder"></i> Delete</a>					
					</td>
				</tr>
				@endforeach  
			</tbody>
		</table>
@endsection

<div id="deleteItem" class="modal model-danger fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Delete Confirmation</h4>
            </div>
            <form action="{{ route('destroy', 'test') }}" method ="post">
                {{method_field('delete')}}
                {{csrf_field()}}
            <div class="modal-body">
                <p class="text-center">Are you sure you want to delete this?</p>
                <input type="hidden" class="deleteitem-id" value="" name="deleteid">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">No, Cancel</button>
                <button type="submit" class="btn btn-warning" >Yes, Delete</button>
            </div>
        </div>
    </div>
</div>