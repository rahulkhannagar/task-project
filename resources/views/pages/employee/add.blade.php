@extends('layouts.layout')
@section('title', 'Employee :: List')


    @section('content')
	<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                   <img src="{{ url('/img/logo.png') }}" height="50px;">
                </a>
                <a class="navbar-brand" href="{{ action('CompaniesController@index') }}">
                   Company
                </a>
                <a class="navbar-brand" href="{{ action('EmployeesController@index') }}">
                   Employee
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
	<form class="form-horizontal form-label-left" data-parsley-validate method="post" action="{{url('/employee/store')}}">
		{{csrf_field()}}
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">First Name &nbsp;*</label>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<input type="text" name="first_name" id="full_name" value="{{empty($employee->first_name) ? old('first_name') : $employee->first_name}}" class="form-control" placeholder="First Name" required>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				@if (!empty($errors->first('first_name')))
					<span class="label label-danger" style="font-size:12px;">{{$errors->first('first_name')}}</span>
				@endif
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">Last Name &nbsp;*</label>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<input type="text" name="last_name" id="last_name" value="{{empty($employee->last_name) ? old('last_name') : $employee->last_name}}" class="form-control" placeholder="Last Name" required>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				@if (!empty($errors->first('last_name')))
					<span class="label label-danger" style="font-size:12px;">{{$errors->first('last_name')}}</span>
				@endif
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">Company &nbsp;*</label>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<select name="company_id" class="form-control">
				<option>-- Select --</option>
				@foreach ($companies as $company)
				<option value="{{$company->id}}" {{ ( (! empty($employee->company_id)) && ($employee->company_id == $company->id))? "selected": ''}}>{{$company->name}}</option>
				@endforeach	
				</select>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				@if (!empty($errors->first('last_name')))
					<span class="label label-danger" style="font-size:12px;">{{$errors->first('last_name')}}</span>
				@endif
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">Email &nbsp;*</label>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<input type="text" name="email" id="email" value="{{empty($employee->email) ? old('email') : $employee->email}}" class="form-control" placeholder="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				@if (!empty($errors->first('email')))
					<span class="label label-danger" style="font-size:12px;">{{$errors->first('email')}}</span>
				@endif
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">Phone &nbsp;*</label>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<input type="text" name="phone" id="phone" value="{{empty($employee->phone) ? old('phone') : $employee->phone}}" class="form-control" placeholder="Phone" required>
			</div>
			<input type="hidden" name="id" value="{{empty($employee->id) ? "" : $employee->id}}">
			<div class="col-md-3 col-sm-3 col-xs-12">
				@if (!empty($errors->first('phone')))
					<span class="label label-danger" style="font-size:12px;">{{$errors->first('phone')}}</span>
				@endif
			</div>
		</div>
		<input type="submit" class="btn btn-success" >
							
	</form>
@endsection